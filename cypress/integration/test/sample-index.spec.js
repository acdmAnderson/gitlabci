const URL = "http://localhost:3000";

describe("My First Test", function() {
  it("Test Click on button -> Try it out", function() {
    cy.visit(URL);
    cy.get(":nth-child(1) > .button").click();
  });
  it("Test Click on button -> Example 1", function() {
    cy.visit(URL);
    cy.get(":nth-child(2) > .button").click();
  });
  it("Test Click on button -> Example 2", function() {
    cy.visit(URL);
    cy.get(":nth-child(3) > .button").click();
  });
  it("Has image -> Feature One", function() {
    cy.visit(URL);
    cy.get(
      ":nth-child(1) > .wrapper > .gridBlock > :nth-child(1) > .blockImage > img"
    );
  });

  it("Check great info from Blog page", () => {
    cy.visit(URL);
    cy.get(".nav-site > :nth-child(4) > a").click();
    cy.get(".posts > :nth-child(4)").find(
      "a.button"
    ).click();
    cy.get(".postHeaderTitle > a");
  });
});
