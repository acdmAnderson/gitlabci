---
title: Hello Docusauro.io
author: Rodrigo Rocha
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat.

<!--truncated-->

## Agora sim!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
const express = require("express");
const routes = require("./routes");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

const dbUser = process.env.MONGO_USER;
const dbPassword = process.env.MONGO_PASSWORD;
const dbURL = `mongodb+srv://${dbUser}:${dbPassword}@cluster0-zojbg.mongodb.net/week10?retryWrites=true&w=majority`;
//const dbURL = `mongodb://${dbUser}:${dbPassword}@cluster0-shard-00-00-zojbg.mongodb.net:27017,cluster0-shard-00-01-zojbg.mongodb.net:27017,cluster0-shard-00-02-zojbg.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority`
console.log(dbURL);

mongoose.connect(dbURL, {
 useNewUrlParser: true,
 useUnifiedTopology: true
});

app.use(cors());
app.use(express.json());
app.use(routes);

app.listen(3333);

```
